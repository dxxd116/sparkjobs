package samples

import com.vividsolutions.jts.io.WKTReader

object PointTest {
  
  
  def main(args:Array[String]){
    
    val reader=new WKTReader();
    val s="POLYGON((141.1083984375 -29.130570984840688,154.0283203125 -29.130570984840688,154.0283203125 -39.21310328979648,141.1083984375 -39.21310328979648,141.1083984375 -29.130570984840688))";  
    val state=reader.read(s);
    
    val pStr="POINT(123.135145 -38.025692)";
    val p=reader.read(pStr);
    
    if (state.covers(p)){
      println("State contains the point!")
    }
    else{
      println("State does not contain the point!");
    }
      
  }
  
}