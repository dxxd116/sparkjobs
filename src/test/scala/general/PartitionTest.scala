package general

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

object PartitionTest {
  
  def main(args:Array[String]){
  
    if (args.length ==0){
      System.err.println("Usage: java -jar <application.jar> <filename.csv>")
      System.exit(1);
    }
  val fileName=args(0)
  val conf=new SparkConf().setAppName("Partition Test").setMaster("local[4]");
  val sc=new SparkContext(conf);
  
  val rdd=sc.textFile(fileName);
  println(rdd.partitions.length + " partitions!");
  
  }
  
}