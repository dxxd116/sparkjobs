package general

object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  var a=0;                                        //> a  : Int = 0
  for (a <- 0 until 2){
  println(a)                                      //> 0
                                                  //| 1
  }
}