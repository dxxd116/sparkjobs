package general

import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import scala.collection.mutable.ListBuffer
import org.junit.Test


object DateTest {
    
  def main(args:Array[String])
  {
    
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val tod=new DateTime(2014,9,8,10,20,20);
    println(outputDatetimePattern.print(tod));
    
    val dat2=new DateTime(2014,9,8,20,10,10);
    println(dat2.secondOfDay().get()-tod.secondOfDay().get());
    
    println(getDatesBetween(tod,dat2));
    
    getQuarter("2015-12-30")
  }
  
  
  def getDatesBetween( startDate :  DateTime, endDate:DateTime):(List[String],List[Int])={
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val dates=ListBuffer.empty[String];
    val hours=ListBuffer.empty[Int];
    
    
     var datetime=startDate.minusMinutes(startDate.minuteOfHour().get()).minusSeconds(startDate.secondOfMinute().get())  
    while (datetime.compareTo(endDate) <=0 ){
      hours += datetime.hourOfDay().get();
      
      dates +=outputDatetimePattern.print(datetime);
      datetime=datetime.plusHours(1);
    }
    return (dates.toList,hours.toList);
  }
  def getQuarter(dateStr:String)={
    //var dateStr="2015-09-30"
    val parts=dateStr.split("-");
    val year=parts(0).toInt
    val month=parts(1).toInt
    val day=parts(2).toInt
    
    val quarter=(month-1)/3+1
    println(dateStr+ ":\tQ"+quarter)
  }
}