package spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.vividsolutions.jts.geom.GeometryFactory
import com.vividsolutions.jts.io.WKTReader
import com.vividsolutions.jts.geom.Coordinate

object TestJob {
  
  private  val factory=new GeometryFactory();
  
  def main(args:Array[String])
  {
    println("Hello, world!");
    
    var path="/user/ec2-user/profile/homework.csv";
    //path="file://D:/work/singtel/optus/indexing/profile/sampleHomeWork.csv";
    val rdd=getRDD(path);
    
    println(rdd.count() + " lines found!");
    
    def mapFunc(s:String):Tuple2[String , (String,String)] ={
      
      val b=s.split("\t");
      val c=new Tuple2(b.apply(0),(b.apply(1),b.apply(2)))
      
      return c;    
    }
    
    val rdd2=rdd.map { mapFunc} 
    
    def printOut(k:(String,(String,String)))={
                 
          print(k._1 + "\t=>\t") 
          val c=k._2;
          //print(c._1 + "\t" + c._2 + "\t");
          if (findState(c._1))
          {
            print("Home is in State!\t" + c._1 +"\t")
          }
          if (findState(c._2))
          {
            print("Work is in State!\t"+ c._2+"\t")
          }
          println()
      
    }
    
    
    //rdd2.reduce((a,b) => {a});
    rdd2.take(50).foreach(printOut)
    
    println(rdd2.count() + " lines after reduce!");
  }

  def getRDD(path:String):RDD[String]={
      val conf=new SparkConf().setAppName("Local Indexing Job").setMaster("yarn-client")
      val sc=new SparkContext(conf);
      val rdd=sc.textFile(path);
      return rdd;
  }

  
  
  def findState(pStr:  String):Boolean={
    
    val reader=new WKTReader();
    val s="POLYGON((141.1083984375 -29.130570984840688,154.0283203125 -29.130570984840688,154.0283203125 -39.21310328979648,141.1083984375 -39.21310328979648,141.1083984375 -29.130570984840688))";  
    val state=reader.read(s);
    
    //pStr="POINT(123.135145 -38.025692)";
    if (pStr.equalsIgnoreCase("NA")){
      return false;
    }
    val parts=pStr.split(",");
    val d1=parts.apply(0).toDouble;
    val d2=parts.apply(1).toDouble;
    val coord=new Coordinate(d2,d1);
       
    val p=factory.createPoint(coord);
    
    if (state.covers(p)){
      //println("State contains the point!")
      return true;
    }
    else{
      //println("State does not contain the point!");
      return false;
    }
  }
  
  
}