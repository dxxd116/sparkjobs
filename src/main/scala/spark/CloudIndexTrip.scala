package spark

import java.util.UUID
import scala.collection.JavaConverters.bufferAsJavaListConverter
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer
import scala.io.Source
import org.apache.http.impl.client.SystemDefaultHttpClient
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import utils.IndexerUtils

class CloudIndexTrip {
   private val submtzMap=getSubmtzs();
   val cl = new SystemDefaultHttpClient();
   private val solrClient=new CloudSolrClient("52.76.76.186,52.76.80.91,52.76.58.188:2181",cl);
   private val inputFormatter=DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
   private val outputFormatter=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
   
   
  def getSubmtzs():HashMap[Int,ListBuffer[Int]]={
    val submtzMap=new HashMap[Int,ListBuffer[Int]]();
    val src=Source.fromInputStream(getClass.getResourceAsStream("/submtzs.csv"))
    src.getLines().filter(x=> !x.split(",")(0).isEmpty()).foreach(line => {
      
      val parts=line.split(",")
      
      val submtzId=parts(0).toInt
      val mappings=new ListBuffer[Int]();
      var subzone=parts(3)
      var planningArea=parts(6)
      var planningRegion=parts(9)
      if (subzone.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(3).toInt    //sub-zone
      }
      if (planningArea.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(6).toInt    //Planning-area
      }
      if (planningRegion.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(9).toInt    //Planning-region
      }
      
      submtzMap+=(submtzId->mappings)
    })
    
    return submtzMap
    }
  
  def indexJobForRoutes(collectionName:String,dataList:Iterator[(String,Iterable[Array[String]])])={
    val documents=new ListBuffer[SolrInputDocument]();
    val threshold=1000
    while (dataList.hasNext){
      val data=dataList.next();
      val imsi=data._1
      val docs=data._2
      val masterDoc=new SolrInputDocument();
      masterDoc.addField("id",UUID.randomUUID().toString());
      masterDoc.addField("imsi",imsi)
      documents+=(masterDoc);
      
      val childDocuments=getChildren(docs);
      masterDoc.addChildDocuments(childDocuments)
      if (documents.size >= threshold){
        solrClient.add(collectionName,documents.asJava)
        //solrClient.commit(collectionName,true,true)
        documents.clear()
       println(threshold + " documents indexed!") 
      }  
    }
    
    if (documents.size>0){
        solrClient.add(collectionName,documents.asJava);
        solrClient.commit(collectionName,true,true);
      }
              
  }
  
  def getChildren(docs:Iterable[Array[String]]):java.util.List[SolrInputDocument]={
    val children=new ListBuffer[SolrInputDocument]();
    for (doc <- docs){
      var submtzId=0;
      if (doc(0).contains("-")){
        //submtzId=doc(0).split("-")(0).toInt
      }
      else
      {
        submtzId=doc(0).toInt
      }
      val mappings=submtzMap.get(submtzId).getOrElse(ListBuffer(0,0,0))
      val subzoneId=mappings(0);
      val planningAreaId=mappings(1)
      val planningRegionId=mappings(2)
      val dwellStartDateTime=inputFormatter.parseDateTime(doc(1))
      val dwellEndDateTime=inputFormatter.parseDateTime(doc(2))
      val dwellDuration=(dwellEndDateTime.getMillis-dwellStartDateTime.getMillis)/1000
      val calculatedTimes=getDatesBetween(dwellStartDateTime,dwellEndDateTime)
      val transportMode=doc(3)
      var transportStartDateTime:DateTime=null;
      if (!doc(4).isEmpty) {
        transportStartDateTime=inputFormatter.parseDateTime(doc(4))
      }
      val transportStartPlace=doc(5)
      var transportEndDateTime:DateTime=null;
      if (!doc(6).isEmpty()){
      transportEndDateTime=inputFormatter.parseDateTime(doc(6))
      }
      val transportEndPlace=doc(7)
   
      val child=new SolrInputDocument();
      child.addField("id",UUID.randomUUID().toString());
      
      
      child.addField("visit_start_tdt",outputFormatter.print(dwellStartDateTime))
      child.addField("start_ti",dwellStartDateTime.getHourOfDay)
      child.addField("visit_end_tdt",outputFormatter.print(dwellEndDateTime))
      child.addField("end_ti",dwellEndDateTime.getHourOfDay)
      child.addField("duration_ti",dwellDuration)
      child.addField("submtz_ti",submtzId)
      child.addField("subzone_ti",subzoneId);
      child.addField("plan_area_ti",planningAreaId);
      child.addField("plan_region_ti",planningRegionId);
      child.addField("visit_stay_tdts",calculatedTimes._1)
      child.addField("hour_tis",calculatedTimes._2)
      if (!transportMode.isEmpty()){
        child.addField("transport_mode_s",transportMode)
      }
      if (transportStartDateTime!=null){
        child.addField("transport_start_tdt",outputFormatter.print(transportStartDateTime))
      }
      if (!transportStartPlace.isEmpty()){
        child.addField("transport_start_place_s",transportStartPlace)
      }
      if (transportEndDateTime!=null){
        child.addField("transport_end_tdt",outputFormatter.print(transportEndDateTime))
      }
      if (!transportEndPlace.isEmpty()){
        child.addField("transport_end_place_s",transportEndPlace)
      }
      
      children.+=(child)
    }
    
    return children.asJava
  }
  
  def getDatesBetween( startDate :  DateTime, endDate:DateTime):(java.util.List[String],java.util.List[Int])={
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val dates=new ListBuffer[String]();
    val hours=new ListBuffer[Int];
    
    
    //var datetime=startDate.minusMinutes(startDate.minuteOfHour().get()).minusSeconds(startDate.secondOfMinute().get())
    var datetime=startDate
    while (datetime.compareTo(endDate) <=0 ){
      val hour=datetime.hourOfDay().get()
      if (!hours.contains(hour)){
        hours +=  datetime.hourOfDay().get();
      }
      
      dates += outputDatetimePattern.print(datetime);
      datetime=datetime.plusMinutes(15);
    }
    return (dates.asJava,hours.asJava);
  }
  
  def deleteAllFromCollection(collectionName:String)={
    solrClient.deleteByQuery(collectionName,"*:*")
  }
  
  def closeSolrConnection()={
    solrClient.close()
  }
  def optimizeCollection(collectionName:String)={
    solrClient.optimize(collectionName, true, true)
  }
}


object CloudIndexTrip{
   private val files=new HashMap[String,String]();
   files+=("2015Q2" -> "/imsi_dorm_marmay2015.txt");
   files+=("2015Q3" -> "/imsi_dorm_julsep2015.txt");
   
   private var sparkContext:SparkContext=null
   
   def getFornaxImsis(quarter:String):HashSet[String]={
     val imsis=new HashSet[String]();
     val fileName=files.get(quarter).get
     val src=Source.fromInputStream(getClass.getResourceAsStream(fileName));
     src.getLines().foreach(x => {
       val parts=x.split("\t");
       imsis.+=(parts(0))
     })
     
     return imsis;
  }
   
   
   def setSparkContext(env:String,appName:String)={
     val sparkConf=new SparkConf();
     sparkConf.setAppName(appName);
     if (env=="local"){
       sparkConf.setMaster("local[4]")
     }
     else{
       sparkConf.setMaster("yarn-cluster")
     }
     
     sparkContext=new SparkContext(sparkConf);
     
   }
   
   def getRDD(fileName:String):RDD[String]={
     val rdd=sparkContext.textFile(fileName);
     return rdd
   }
   
   def closeSparkContext={
     sparkContext.stop();
   }
   
   def getSolrClient():CloudSolrClient={
     
     val cl = new SystemDefaultHttpClient();
     val solrClient=new CloudSolrClient("52.76.76.186,52.76.80.91,52.76.58.188:2181",cl);
     return solrClient  
   }
   
  def mapFunction(line:String):(String,Array[String])={
    val parts=line.split(",",9)
    return (parts(0),parts.drop(1));
  }
  
  def main(args:Array[String])={
    val startTime=System.currentTimeMillis();
    var dateStr="2015-09-30"
    if (args.length>0){
      dateStr=args(2)
    }
    else
    {
      System.out.println("Usage: java -jar <application.jar> <local|cloud> <fileName> <Date String in the format of yyyy-MM-dd>");
      System.exit(1)
    }
        
    val parts=dateStr.split("-");
    val year=parts(0).toInt
    val month=parts(1).toInt
    val day=parts(2).toInt
    
    val quart=(month-1)/3+1
    val quarter="Q"+quart;
    val imsis=getFornaxImsis("2015"+quarter);
    println(imsis.size + " imsis found for " + quarter  );
    val collectionName="MHATrips"+year+month.formatted("%02d")+day.formatted("%02d")
        
    var fileName="D:/work/singtel/MHA/fornax/output.txt";
    if (args.length>0){
      setSparkContext("local","Indexing Trip")
      fileName=args(1)
    }else{
      setSparkContext("local","Indexing Trip")
    }
    val rdd=getRDD(fileName);
    
    val filteredRdd=rdd.filter(x => {
      val parts=x.split(",")
      imsis.contains(parts(0));
    })

    println(rdd.count()+ " trips found!")
    println(filteredRdd.count()+ " filtered trips found!")
    
    val rdd3=filteredRdd.map(mapFunction).groupByKey()
    println(rdd3.count()  + " final imsis found!")
    val solrClient=getSolrClient()
    //IndexerUtils.createCollection(solrClient, collectionName, "MHA_Trips", "52.74.232.128:8983_solr,52.74.43.159:8983_solr")
    solrClient.deleteByQuery(collectionName,"*:*")
    solrClient.close();
    
    rdd3.foreachPartition(x => {val proc=new CloudIndexTrip(); 
      proc.indexJobForRoutes(collectionName,x);
      proc.optimizeCollection(collectionName);
      proc.closeSolrConnection})

    closeSparkContext;
    val endTime=System.currentTimeMillis()
    println((endTime-startTime)/1000 + " seconds was spent in total!")
  }
  
  
  
}