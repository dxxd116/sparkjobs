package spark

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.PairRDDFunctions
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.common.SolrInputDocument

object LocalIndexigJob {
  
  private val solrClient=getSolrClient("http://52.74.153.22:8983/solr/profile/");
  def main(args:Array[String]){
    
    var path="/user/ec2-user/profile/homework.csv";
    path="D:/work/singtel/optus/indexing/profile/sampleHomeWork.csv";
    val rdd=getRDD(path);
    
    println(rdd.count() + " lines found!");
    
    
    val rdd2=rdd.map { mapFunc}  
    
    val rdd3=rdd2.groupByKey();
    
    rdd3.foreach(s => println(s._1 + "\t=>\t" + s._2.size));
    rdd3.foreach(s => println(s._1 + "\t=>\t" + s._2.head.mkString("||")));
   
    //rdd2.collectAsMap().take(200).foreach(indexDocument)
    
    
    //println("Optimizing collection ...");
    //solrClient.optimize();
    
    solrClient.close();
  }

  def printOut(a: (String,Array[String]))={
    
    println(a._1 + "\t=>\t" + a._2.mkString("||"))  
  }
  
  
  def mapFunc(s:String):(String,Array[String]) ={
      val b=s.split("\t");
      val c=(b(0),b.drop(1))
      
      return c;    
    }
   
  
  def getSolrClient(address: String):SolrClient={
    val sClient=new HttpSolrClient(address);
    return sClient;
  }
  
  def indexDocument(arr:(String,Array[String]))={
    
    val a=new SolrInputDocument();
    
    a.addField("id",arr._1);
    a.addField("imsi", arr._1);
    a.addField("homeLocation_s",arr._2.apply(0));
    a.addField("workLocation_s",arr._2.apply(1));
    
    solrClient.add(a);
    
  }
  
  def getRDD(path:String):RDD[String]={
      val conf=new SparkConf().setAppName("Local Indexing Job").setMaster("local[3]")
      val sc=new SparkContext(conf);
      val rdd=sc.textFile(path);
      return rdd;
  }
  
}