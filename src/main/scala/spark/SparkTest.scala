package spark

import org.apache.spark.api.java.JavaRDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import scala.collection.mutable.LinkedList
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.api.java.function.PairFlatMapFunction
import java.io.Serializable
import collection.JavaConverters._
import org.apache.spark.api.java.function.VoidFunction

object Test {
  
    val conf=new SparkConf().setMaster("local[4]").setAppName("Standalone Test");
    val sc=new SparkContext(conf);  
    val rdd=sc.parallelize(List(1,2,55,332,12));
    val javaRdd=new JavaRDD(rdd); 
    val rdd2=sc.parallelize(List(2,3,4,198,2,12));
      
  def main(args : Array[String]){

    FaltMapToPairTest();
    sc.stop();
  }
  
      
  
  def FaltMapToPairTest(){
    val rdd=sc.parallelize(Array("fd,sa,test2,fdsfa,req","fda,rewq ,plkkf,jdsa,ofds"));
    val javaRdd=new JavaRDD(rdd);
    rdd.foreach { x:String => {println(x)} }
    
    class myClass extends PairFlatMapFunction[String,String,String] {
           
      def call(s:String) : java.lang.Iterable[(String,String)]={
      var arr=ArrayBuffer.empty[(String,String)];
      val a=s.split(",")
      for (i <- Range(0 ,a.length-1,2) )
      {
         println(i + " is processed!")
         val b=(a.apply(i),a.apply(i+1))
         arr += b;
      }
      return arr.toIterable.asJava;
      
      };
      
    }
    
    val pairs=javaRdd.flatMapToPair(new myClass());
    
    class javaPrintln extends VoidFunction[Tuple2[String,String]]{
     def call(x:Tuple2[String,String]){
       println(x._1 + "\t=>\t" + x._2)
       };
    }
      pairs.foreach(new javaPrintln());
    
    
  }
  
    def FlatMapTest(){
          
      val rdd3= rdd.union(rdd2);
      val d=rdd3.collect();
    
      val e=d.flatMap { x => Array(x,x*2) }
      
    }
  
  
}