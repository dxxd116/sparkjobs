package spark

import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import org.apache.solr.common.SolrInputDocument
import org.joda.time.format.DateTimeFormat
import collection.JavaConverters._
import java.util.UUID
import java.util.ArrayList
import org.joda.time.DateTime
import scala.collection.mutable.ListBuffer
import org.apache.http.impl.client.SystemDefaultHttpClient

object CloudIndexJobFootfall {
 
  private val solrClient:CloudSolrClient=getCloudSolrClient("52.65.54.15,52.65.103.71,52.65.103.73:2181");
  private val inputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
  private val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
  private val collectionName="test_collection";
  
  def main(args: Array[String])={
    
    if (args.size ==0 ) 
    {
      println("Usage : java -jar SparkJobs.jar <footfall.csv>")
      System.exit(1);
    }
    
      
    val startTime=System.currentTimeMillis();
    //val fileName="D:/work/Singtel/files/areavisit/sampleFootfallOptus.csv";
    val fileName=args(0);
    val conf=new SparkConf().setAppName("Cloud Indexing Job")
    if (fileName.startsWith("file://")){
      conf.setMaster("local[4]");
    }
    else{
      conf.setMaster("yarn-client");
    }
    
    val sc=new SparkContext(conf);
    val lines=sc.textFile(fileName);
    
    
    val pairRdd=lines.map( x => (x.split(",")(0),  x))
    val groupedRdd=pairRdd.groupByKey();
    pairRdd.take(50).foreach(a => println(a._1 + "\t=>" + a._2));
    groupedRdd.take(50).foreach(a => println(a._1 + "=>\t"+a._2.mkString("||")));
   
    println(groupedRdd.partitions.length + " partitions in total!")
    println("Start to index documents...");
    solrClient.deleteByQuery(collectionName,"*:*");
    solrClient.commit(collectionName,true,true);
    
    
  
    groupedRdd.foreachPartition(indexJob)
    
    println("Start to optimizing Index!")
    val t1=System.currentTimeMillis()
    solrClient.optimize(collectionName,true,true);
    val t2=(System.currentTimeMillis()-t1)/1000
    println(t2 + " seconds in optimization!")
    
    solrClient.close();
    val endTime=System.currentTimeMillis();
    println((endTime-startTime)/1000 + " seconds in indexing footfall!");
    
  }
  
  
  
  def getCloudSolrClient(zkhost: String):CloudSolrClient={
    val cl = new SystemDefaultHttpClient();
    val solrClient=new CloudSolrClient(zkhost,cl);
    return solrClient;
  }
  
  def indexJob(datas:Iterator[(String,Iterable[String])])={
    val documents=new ArrayList[SolrInputDocument]();
    
    while (datas.hasNext){
      val data=datas.next();
      val imsi  =data._1;
      val arr   =data._2;
    
      val masterDoc=new SolrInputDocument();
      masterDoc.addField("id",UUID.randomUUID().toString());
      masterDoc.addField("imsi",imsi);
    
      //var children=scala.collection.mutable.ListBuffer.empty[SolrInputDocument];
      var children=   getChildren(arr);
      masterDoc.addChildDocuments(children)
   
      documents.add(masterDoc);
      if (documents.size() >=30000){
        solrClient.add(collectionName,documents);
        //solrClient.commit(collectionName,true,true);
        documents.clear();
        println("30000 documents indexed!");
      }
      }
    
    
      if (documents.size()>0){
        solrClient.add(collectionName,documents);
        solrClient.commit(collectionName,true,true);
      }
          
         
    
  }
  
  def getChildren(lines:Iterable[String]):ArrayList[SolrInputDocument]={
      var children=new ArrayList[SolrInputDocument]();
      for (line <- lines){ 
      val parts=line.split(",");
      
      val imsi=parts(0);
      val areaId=parts(1);
      val startDate=inputDatetimePattern.parseDateTime(parts(2));
      val endDate=inputDatetimePattern.parseDateTime(parts(3));
      
      val duration=endDate.secondOfDay().get()-startDate.secondOfDay().get();
      
      val dates=getDatesBetween(startDate,endDate);
      val stayDates=dates._1;
      val hours=dates._2;
      
      val child =new SolrInputDocument();
      child.addField("id",UUID.randomUUID().toString());
      child.addField("sa2_s",areaId);
      child.addField("visit_start_tdt",outputDatetimePattern.print(startDate));
      child.addField("visit_end_tdt",outputDatetimePattern.print(endDate));
      child.addField("visit_stay_tdts",stayDates);
      child.addField("hour_tis",hours);
  
      children.add(child);
      }
      return children;
      
    }
  
  
  def getDatesBetween( startDate :  DateTime, endDate:DateTime):(ArrayList[String],ArrayList[Int])={
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val dates=new ArrayList[String]();
    val hours=new ArrayList[Int];
    
    
    var datetime=startDate.minusMinutes(startDate.minuteOfHour().get()).minusSeconds(startDate.secondOfMinute().get())  
    while (datetime.compareTo(endDate) <=0 ){
      hours add  datetime.hourOfDay().get();
      
      dates add outputDatetimePattern.print(datetime);
      datetime=datetime.plusHours(1);
    }
    return (dates,hours);
  }

  
  def convertDate(s:String):String={
      val dat=inputDatetimePattern.parseDateTime(s);
      //dat.secondOfMinute=0;
      val outputString=outputDatetimePattern.print(dat);
      
      return outputString;
    }
  
    def getRDD(path:String):RDD[String]={
      val conf=new SparkConf().setAppName("Cloud Indexing Job")
      val sc=new SparkContext(conf);
      val rdd=sc.textFile(path);
      return rdd;
  }
  
}