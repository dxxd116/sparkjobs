package spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.hadoop.fs.FileSystem
import org.apache.http.impl.client.SystemDefaultHttpClient
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.hadoop.fs.Path
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime

object CloudIndexTripFromLocal {
  private var sparkContext:SparkContext=null
  def main(args:Array[String])={
    val startTime=System.currentTimeMillis()
    
    if (args.length>0){
    }
    else   {
      System.out.println("Usage: java -jar <application.jar> <local|cloud> <workingDir> <Date String in the format of yyyyMMdd> <number of days to index!>");
      System.exit(1)
    }
    setSparkContext("local","Indexing Trip")
    val solrClient=getSolrClient()
    
    val workingDir=args(1);
    val dateStr=args(2)
    val numOfDays=args(3).toInt
    
    val year=dateStr.substring(0,4).toInt
    val month=dateStr.substring(4,6).toInt
    val day=dateStr.substring(6,8).toInt
    var i=0;
    
    val startDate=new DateTime(year,month,day,0,0);
    for ( i <- 0 until numOfDays){
      val dateTime=startDate.plusDays(i)
    
      val suffix=dateTime.getYear.formatted("%04d")+dateTime.getMonthOfYear.formatted("%02d")+dateTime.getDayOfMonth.formatted("%02d")
    
      val collectionName="MHATrips"+suffix
        
      val fileName=workingDir+"footfall"+suffix+".txt";
        
    val hadoopConfiguration=sparkContext.hadoopConfiguration
    val hdfs=FileSystem.get(hadoopConfiguration);
    val filePath =new Path(fileName);
    
    var rdd:RDD[String]=null;
    if (hdfs.exists(filePath)) {
      rdd=sparkContext.textFile(fileName);
    }else{
      rdd=sparkContext.emptyRDD[String];
      println(fileName + " not found!")
    }        

    println(rdd.count()+ " trips found!")
    
    val rdd3=rdd.map(mapFunction).groupByKey()
    println(rdd3.count()  + " final imsis found!")
    
    solrClient.deleteByQuery(collectionName,"*:*")
        
    rdd3.foreachPartition(x => {
      val proc=new CloudIndexTrip(); 
      proc.indexJobForRoutes(collectionName,x);
      //proc.optimizeCollection(collectionName);
      proc.closeSolrConnection})
      
      solrClient.optimize(collectionName, true, true)
      println(collectionName + " is optimized!")
    }
    
    solrClient.close();
    closeSparkContext;
    
    val endTime=System.currentTimeMillis()
    println((endTime-startTime)/1000 + " seconds was spent in total!")
    
  }
  
    def closeSparkContext={
     sparkContext.stop();
   }
  
   def setSparkContext(env:String,appName:String)={
     val sparkConf=new SparkConf();
     sparkConf.setAppName(appName);
     if (env=="local"){
       sparkConf.setMaster("local[8]")
     }
     else{
       sparkConf.setMaster("yarn-client")
     }
     
     sparkContext=new SparkContext(sparkConf); 
   }
   
    def getSolrClient():CloudSolrClient={
     
     val cl = new SystemDefaultHttpClient();
     val solrClient=new CloudSolrClient("52.76.76.186,52.76.80.91,52.76.58.188:2181",cl);
     return solrClient  
   }
   
    def mapFunction(line:String):(String,Array[String])={
     val parts=line.split(",",9)
     return (parts(0),parts.drop(1));
  }
}