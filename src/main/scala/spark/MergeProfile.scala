package spark

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.hadoop.fs.FileSystem
import java.io.IOException
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.compress.CompressionCodec
import org.apache.hadoop.io.compress.BZip2Codec
import org.apache.hadoop.mapreduce.OutputFormat
import org.apache.hadoop.io.compress.GzipCodec
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat




class MergeProfile() {
  
  private var workingDir="";
  private val conf=new SparkConf().setAppName("Merge Profile Job");
  private var context:SparkContext=null;
  
  
  def setSparkContext(env : String )={
    if (env=="local")
    {
      conf.setMaster("local")
    }else
    {
      conf.setMaster("yarn-client")
    }
    context=new SparkContext(conf)
    
  }
  
  def setWorkingDir(dirName:String)={
    if (dirName.endsWith("/")){
      this.workingDir=dirName;
    }else{
      this.workingDir=dirName + "/";
    }
  }
  
  def getRdd(fileName:String):RDD[String]={
    context.textFile(fileName);  
  }
  
  
  def mergeFiles()={
    var parts:Array[String]=null;
    val locals=getRdd(workingDir + "profiles_local.csv").map(line => {
      parts=line.split("\t");
     (parts(0), parts.drop(1).mkString("\t"))
    }).filter( x=>x._1 != "imsi")
    
    val elders=getRdd(workingDir+"elder_list.txt").map(line => (line,"elder"))
    elders.take(10).foreach(x => println(x))
    
    val housewives=getRdd(workingDir+"housewife_list.txt").map(line => (line,"housewife"));
    
    /*val elderAndHouseWife=elders.join(housewives);
    println(elderAndHouseWife.count() + " elders who are also housewives!")
    elderAndHouseWife.take(10).foreach(println);*/
    
    
    val transportModes=getRdd(workingDir+"imsi_transportModes.txt").map(line => {
      parts=line.split("\t")
      (parts(0),parts(1))      
    })
    
    val finalImsis=locals.leftOuterJoin(elders).map(concateValues)
     .leftOuterJoin(housewives).map(concateValues)
     .leftOuterJoin(transportModes).map(concateValues);
    
    val hdfsConf=context.hadoopConfiguration;
    val fs=FileSystem.get(hdfsConf)
    
    val outputFile=workingDir+ "mergedProfile"
    try {fs.delete(new Path(outputFile),true)}
    catch
    {
      case ex:IOException => {println(ex.getMessage()); println("Please try to solve the error First!")}
      case _:Throwable => println("No specific error!" )
    }
      
    //val bzip2Codec=new BZip2Codec();
    //finalImsis.saveAsTextFile(outputFile,classOf[BZip2Codec]);
    //finalImsis.saveAsTextFile(outputFile,classOf[GzipCodec]);
    //finalImsis.saveAsTextFile(outputFile);
    hdfsConf.set("mapreduce.output.textoutputformat.separator","\t");
    finalImsis.saveAsNewAPIHadoopFile(outputFile,classOf[String],classOf[String],classOf[TextOutputFormat[String,String]],hdfsConf);
  }
  
  
  def concateValues(x:(String,(String,Option[String]))):(String,String) ={
      var str:String="";
      if (x._2._2.isDefined){
        str=x._2._1 + "\t" + x._2._2.get
        }
      else{
        str=x._2._1+"\t"+"NA"
        }
       (x._1 , str)
  }
  
  
}



object MergeProfile {
  
  def main(args:Array[String])={
    
    println("Running profile merging job...")
    //val merger=new MergeProfile("local") with Serializable;
    val merger=new MergeProfile() with Serializable;
    
    //merger.setWorkingDir("D:/work/singtel/files/profile-locals")
    if (args.size ==0){
      System.err.println("Usage: java -cp <Application.jar> <Working Directory>");
      System.exit(1);
    }
    
    if (args(0).startsWith("file:"))
    {
       merger.setSparkContext("local") 
    }else{
       merger.setSparkContext("yarn-client")
    }
    
    merger.setWorkingDir(args(0));
    merger.mergeFiles();
    
    
  }
}