package spark

import java.util.UUID

import scala.collection.JavaConverters._
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer
import scala.io.Source

import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


class LocalIndexTrip {
   private val submtzMap=getSubmtzs();
   private val solrClient=new HttpSolrClient("http://52.74.153.22:8983/solr/routes-MHA-Q3-2015/");
   private val inputFormatter=DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
   private val outputFormatter=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
   
   
  def getSubmtzs():HashMap[Int,ListBuffer[Int]]={
    val submtzMap=new HashMap[Int,ListBuffer[Int]]();
    val src=Source.fromInputStream(getClass.getResourceAsStream("/submtzs.csv"))
    src.getLines().filter(x=> !x.split(",")(0).isEmpty()).foreach(line => {
      
      val parts=line.split(",")
      
      val submtzId=parts(0).toInt
      val mappings=new ListBuffer[Int]();
      var subzone=parts(3)
      var planningArea=parts(6)
      var planningRegion=parts(9)
      if (subzone.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(3).toInt    //sub-zone
      }
      if (planningArea.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(6).toInt    //Planning-area
      }
      if (planningRegion.equals("N.A.")){
        mappings+=0
      }else{
        mappings+=parts(9).toInt    //Planning-region
      }
      
      submtzMap+=(submtzId->mappings)
    })
    
    return submtzMap
    }
  
  def indexJobForRoutes(dataList:Iterator[(String,Iterable[Array[String]])])={
    val documents=new ListBuffer[SolrInputDocument]();
    val threshold=1000
    while (dataList.hasNext){
      val data=dataList.next();
      val imsi=data._1
      val docs=data._2
      val masterDoc=new SolrInputDocument();
      masterDoc.addField("id",UUID.randomUUID().toString());
      masterDoc.addField("imsi",imsi)
      documents+=(masterDoc);
      
      val childDocuments=getChildren(docs);
      masterDoc.addChildDocuments(childDocuments)
      if (documents.size >= threshold){
        solrClient.add(documents.asJava)
        solrClient.commit(true,true)
        documents.clear()
       println(threshold + " documents indexed!") 
      }  
    }
    
    if (documents.size>0){
        solrClient.add(documents.asJava);
        solrClient.commit(true,true);
      }
              
  }
  
  def getChildren(docs:Iterable[Array[String]]):java.util.List[SolrInputDocument]={
    val children=new ListBuffer[SolrInputDocument]();
    for (doc <- docs){
      var submtzId=0;
      if (doc(0).contains("-")){
        //submtzId=doc(0).split("-")(0).toInt
      }
      else
      {
        submtzId=doc(0).toInt
      }
      val mappings=submtzMap.get(submtzId).getOrElse(ListBuffer(0,0,0))
      val subzoneId=mappings(0);
      val planningAreaId=mappings(1)
      val planningRegionId=mappings(2)
      val dwellStartDateTime=inputFormatter.parseDateTime(doc(1))
      val dwellEndDateTime=inputFormatter.parseDateTime(doc(2))
      val dwellDuration=(dwellEndDateTime.getMillis-dwellStartDateTime.getMillis)/1000
      val calculatedTimes=getDatesBetween(dwellStartDateTime,dwellEndDateTime)
      val transportMode=doc(3)
      var transportStartDateTime:DateTime=null;
      if (!doc(4).isEmpty) {
        transportStartDateTime=inputFormatter.parseDateTime(doc(4))
      }
      val transportStartPlace=doc(5)
      var transportEndDateTime:DateTime=null;
      if (!doc(6).isEmpty()){
      transportEndDateTime=inputFormatter.parseDateTime(doc(6))
      }
      val transportEndPlace=doc(7)
   
      val child=new SolrInputDocument();
      child.addField("id",UUID.randomUUID().toString());
      
      child.addField("visit_start_tdt",outputFormatter.print(dwellStartDateTime))
      child.addField("visit_end_tdt",outputFormatter.print(dwellEndDateTime))
      child.addField("duration_ti",dwellDuration)
      child.addField("submtz_ti",submtzId)
      child.addField("subzone_ti",subzoneId);
      child.addField("planning_area_ti",planningAreaId);
      child.addField("planning_region_ti",planningRegionId);
      child.addField("visit_stay_tdts",calculatedTimes._1)
      child.addField("hour_tis",calculatedTimes._2)
      
      child.addField("transport_start_tdt",outputFormatter.print(transportStartDateTime))
      child.addField("transport_start_place_s",transportStartPlace)
      child.addField("transport_end_tdt",outputFormatter.print(transportEndDateTime))
      child.addField("transport_end_place_s",transportEndPlace)
      
      children.+=(child)
    }
    
    return children.asJava
  }
  
  def getDatesBetween( startDate :  DateTime, endDate:DateTime):(java.util.List[String],java.util.List[Int])={
    val outputDatetimePattern=DateTimeFormat.forPattern("YYYY-MM-dd'T'HH:mm:ss'Z'");
    val dates=new ListBuffer[String]();
    val hours=new ListBuffer[Int];
    
    
    var datetime=startDate.minusMinutes(startDate.minuteOfHour().get()).minusSeconds(startDate.secondOfMinute().get())  
    while (datetime.compareTo(endDate) <=0 ){
      hours +=  datetime.hourOfDay().get();
      
      dates += outputDatetimePattern.print(datetime);
      datetime=datetime.plusHours(1);
    }
    return (dates.asJava,hours.asJava);
  }
  
  def deleteAllFromCollection()={
    solrClient.deleteByQuery("*:*")
  }
   
}


object LocalIndexTrip{
  
   private val files=new HashMap[String,String]();
   private var workingDir="D:/work/singtel/MHA/fornax/";
   files+=("2015Q2" -> "imsi_dorm_marmay2015.txt");
   files+=("2015Q3" -> "imsi_dorm_julsep2015.txt");
   
   private var sparkContext:SparkContext=null
   
   def getFornaxImsis(quarter:String):HashSet[String]={
     val imsis=new HashSet[String]();
     val fileName=files.get(quarter).get
     val src=Source.fromFile(workingDir+fileName);
     src.getLines().foreach(x => {
       val parts=x.split("\t");
       imsis.+=(parts(0))
     })
     
     return imsis;
  }
   
   def setWorkingDir(workDir: String)={
    this.workingDir=workDir
  }
   
   def setSparkContext(env:String,appName:String)={
     val sparkConf=new SparkConf();
     sparkConf.setAppName(appName);
     if (env=="local"){
       sparkConf.setMaster("local[4]")
     }
     else{
       sparkConf.setMaster("yarn-client")
     }
     
     sparkContext=new SparkContext(sparkConf);
     
   }
   
   def getRDD(fileName:String):RDD[String]={
     val rdd=sparkContext.textFile(workingDir+fileName);
     return rdd
   }
   
   def closeSparkContext={
     sparkContext.stop();
   }
   
   def getSolrClient():SolrClient={
     val solrClient=new HttpSolrClient("http://52.74.153.22:8983/solr/routes-MHA-Q3-2015/");
     return solrClient  
   }
   
  def mapFunction(line:String):(String,Array[String])={
    val parts=line.split(",",9)
    return (parts(0),parts.drop(1));
  }
  
  def main(args:Array[String])={
    
    val quarter="2015Q3";
    val imsis=getFornaxImsis("2015Q3");
    println(imsis.size + " imsis found for " + quarter  );
    setSparkContext("local","Indexing Trip")
    val rdd=getRDD("tripSample.txt");
    
    val filteredRdd=rdd.filter(x => {
      val parts=x.split(",")
      imsis.contains(parts(0));
    })

    println(rdd.count()+ " trips found!")
    println(filteredRdd.count()+ " filtered trips found!")
    
    val rdd3=filteredRdd.map(mapFunction).groupByKey()
    println(rdd3.count()  + " final imsis found!")
    val solrClient=getSolrClient()
    solrClient.deleteByQuery("*:*")
    solrClient.close();
    
    rdd3.foreachPartition(x => {val proc=new LocalIndexTrip(); proc.indexJobForRoutes(x)})

    closeSparkContext;
    
  }
 
  
}