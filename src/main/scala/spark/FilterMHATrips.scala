package spark

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import scala.collection.mutable.HashSet
import scala.io.Source
import scala.collection.mutable.HashMap
import org.joda.time.DateTime
import java.io.IOException
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path

object FilterMHATrips extends CloudIndexTrip{
  private var sparkContext:SparkContext=null;
  private val files=new HashMap[String,String]();
   files+=("2015Q2" -> "/imsi_dorm_marmay2015.txt");
   files+=("2015Q3" -> "/imsi_dorm_julsep2015.txt");
   
  def main(args:Array[String])={
 
    val startTime=System.currentTimeMillis();
   
    if (args.length>0){
      setSparkContext("yarn","Filtering Trips")
    }
    else
    {
      setSparkContext("local","Filtering Trips")
      System.out.println("Usage: java -jar <application.jar> <year in the format of yyyy> <Month in the format of MM> <day in the format of dd>");
      exit(1)
    }
    
    val hadoopConfiguration=sparkContext.hadoopConfiguration
    val hdfs=FileSystem.get(hadoopConfiguration);
     
    
    val year=args(0).toInt
    val month=args(1).toInt
    val day=args(2).toInt
    
    val startDate=new DateTime(year,month,day,0,0)
    val endDate=startDate.plusMonths(1);
    var workingDate=startDate;
    while (workingDate.isBefore(endDate)){
      
    var quart=(workingDate.getMonthOfYear-1)/3+1
    if (quart==1) quart=2;
    val quarter="Q"+quart;
    val imsis=getFornaxImsis("2015"+quarter);
    println(imsis.size + " imsis found for " + quarter  );
    
    val dateStr=workingDate.getYear+workingDate.getMonthOfYear.formatted("%02d")+workingDate.getDayOfMonth.formatted("%02d")
    val collectionName="MHATrips"+dateStr 
    val fileName="hdfs:///user/ngyibin/20160210_Fornax/outfile13b/"+dateStr;
    println(fileName);
    val filePath=new Path(fileName)
    var rdd:RDD[String]=null;
    if (hdfs.exists(filePath)) {
       rdd=getRDD(fileName);
    }else
    {
       rdd=sparkContext.emptyRDD[String]
    }
    
    val filteredRdd=rdd.filter(x => {
      val parts=x.split(",")
      imsis.contains(parts(0));
    })

    println(rdd.count()+ " trips found!")
    println(filteredRdd.count()+ " filtered trips found!")
    
    val path="hdfs:///user/xingliang/filteredMHATrips/footfall"+dateStr;
    filteredRdd.saveAsTextFile(path);
     
    workingDate=workingDate.plusDays(1);
    }
    
    closeSparkContext;
    val endTime=System.currentTimeMillis()
    println((endTime-startTime)/1000 + " seconds was spent in total!")
    
    
  }
  
  def getFornaxImsis(quarter:String):HashSet[String]={
     val imsis=new HashSet[String]();
     val fileName=files.get(quarter).get
     val src=Source.fromInputStream(getClass.getResourceAsStream(fileName));
     src.getLines().foreach(x => {
       val parts=x.split("\t");
       imsis.+=(parts(0))
     })
     
     return imsis;
  }
  
  def setSparkContext(env:String,appName:String)={
     val sparkConf=new SparkConf();
     sparkConf.setAppName(appName);
     if (env=="local"){
       sparkConf.setMaster("local[4]")
     }
     else{
       sparkConf.setMaster("yarn-client")
     }
     
     sparkContext=new SparkContext(sparkConf);
     
   }
   
   def getRDD(fileName:String):RDD[String]={
         
       sparkContext.textFile(fileName);
     
   }
   
   def closeSparkContext={
     sparkContext.stop();
   }
   
  
}