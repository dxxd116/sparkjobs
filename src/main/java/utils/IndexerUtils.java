package utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient.HttpUriRequestResponse;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Create;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Delete;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.util.NamedList;
import org.joda.time.DateTime;

public class IndexerUtils {
	
	public static void main(String[] args) throws IOException, SolrServerException{
		CloudSolrClient client = new CloudSolrClient("52.76.76.186,52.76.80.91,52.76.58.188:2181");
		//CloudSolrClient client = new CloudSolrClient("52.65.54.15,52.65.103.71,52.65.103.73:2181");
		
		DateTime startDate=new DateTime(2015,5,16,0,0);
		DateTime endDate=new DateTime(2015,9,30,1,0);
		for (DateTime dat=startDate;dat.isBefore(endDate);dat=dat.plusDays(1)){
		String collectionName="MHATrips" + String.format("%04d%02d%02d", dat.getYear(),dat.getMonthOfYear(),dat.getDayOfMonth());
		System.out.println(collectionName);
				
//		HashMap<String,String> params=new HashMap<String,String>();
//		params.put("configName", "MHA_Trips");
//		params.put("nodeSet", "52.77.174.110:8983_solr,52.77.140.23:8983_solr,52.77.61.235:8983_solr,52.77.184.153:8983_solr");
//		params.put("dataDir", "/solr_data/"+collectionName+"/data");
//		createCollection(client,collectionName,params);
		
		String configSet="MHA_Trips";
		String nodeSet="ec2-52-77-174-110.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr,ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr";
		createCollection(client,collectionName,configSet,nodeSet);
		}
		
		
		/*
		 * 
		DateTime startDate=new DateTime(2015,7,1,0,0);
		DateTime endDate=new DateTime(2015,7,3,0,0);
		
		for (;!startDate.isAfter(endDate);startDate=startDate.plusDays(1)){
			String collectionName="building" +String.format("%4d%02d%02d", startDate.getYear(),startDate.getMonthOfYear(),startDate.getDayOfMonth());
			//System.out.println("Deleting documents in " + collectionName);
			//client.deleteByQuery(collectionName, "*:*");
			//client.commit(collectionName, true, true);
			//optimizeCollection(client,collectionName);
			createCollection(client,collectionName,"building_config",3);
			//createCollection(client,collectionName,"building_config",3);
			//deleteCollection(client,collectionName);
		}*/
		//client.commit();
		
		//deleteDocs(client,collectionName);
		
		//String nodeSet="52.74.232.128:8983_solr";//For MHA trips
		//createCollection(client,collectionName,"MHA_Trips",nodeSet);
		String collectionName="";
		String nodeSet="";
		
//		collectionName="profile-Q2-2015";
//		nodeSet="ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr";
//		addReplicaToCollection(client,collectionName,nodeSet);
//		nodeSet="ec2-52-77-174-110.ap-southeast-1.compute.amazonaws.com:8983_solr";
//		addReplicaToCollection(client,collectionName,nodeSet);
//		nodeSet="ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr";
//		addReplicaToCollection(client,collectionName,nodeSet);
//		nodeSet="ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr";
//		addReplicaToCollection(client,collectionName,nodeSet);
		
		/*collectionName="profile-Q3-2015";
		nodeSet="ec2-52-77-184-153.ap-southeast-1.compute.amazonaws.com:8983_solr";
		addReplicaToCollection(client,collectionName,nodeSet);
		nodeSet="ec2-52-77-174-110.ap-southeast-1.compute.amazonaws.com:8983_solr";
		addReplicaToCollection(client,collectionName,nodeSet);
		nodeSet="ec2-52-77-61-235.ap-southeast-1.compute.amazonaws.com:8983_solr";
		addReplicaToCollection(client,collectionName,nodeSet);
		nodeSet="ec2-52-77-140-23.ap-southeast-1.compute.amazonaws.com:8983_solr";
		addReplicaToCollection(client,collectionName,nodeSet);*/

		//createCollection(client,collectionName,"MHA_Trips",nodeSet);
		//deleteCollection(client,collectionName);
//		for (int i=7;i<=12;i++){
//			deleteReplicaFromCollection(client,collectionName,"core_node"+i);
//		}
		client.close();
	}

public static void deleteReplicaFromCollection (SolrClient client, String collection, String replica) throws IOException, SolrServerException {
		
		try {
			SolrRequest listReq=new CollectionAdminRequest.List();
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){
		
			}
			else
			{	
				System.out.println("Collection " + collection + " is not found!");
			}
	    	
		} catch (SolrException e) {System.exit(1);}
		
		
		System.out.println("Delete replica "+replica+" from collection " + collection + " ... ");
		CollectionAdminRequest.DeleteReplica req = new CollectionAdminRequest.DeleteReplica();
		req.setCollectionName(collection);
		req.setShardName("shard1");
		req.setReplica(replica);	
		
		client.request(req,collection);
		
		client.commit(collection,true,true);
	}
	
	public static void addReplicaToCollection (SolrClient client, String collection, String nodeName) throws IOException, SolrServerException {
		
		try {
				SolrRequest listReq=new CollectionAdminRequest.List() ;
				NamedList<Object> qr=client.request(listReq);
			
				List<String> collections=(List<String>) qr.get("collections");
				if (collections.contains(collection)){
				}
				else{	
					System.out.println("Collection " + collection + " is not found!");
				}
	    	
		} catch (SolrException e) {System.exit(1);}
		
		
		System.out.println("Adding replica to collection " + collection + " ... ");
		CollectionAdminRequest.AddReplica req = new CollectionAdminRequest.AddReplica();
		req.setCollectionName(collection);
		req.setNode(nodeName);
		req.setShardName("shard1");
			
		
		client.request(req,collection);
		
		client.commit(collection,true,true);
	}
public static void deleteCollection(CloudSolrClient client,String collectionName) throws SolrServerException, IOException {
		// TODO Auto-generated method stub
	
	Delete deleteReq = new Delete();
	deleteReq.setCollectionName(collectionName);
	client.request(deleteReq);
	//client.commit(collection,true, true);
	System.out.println(collectionName + " deleted. \n");
		
	}

public static void optimizeCollection(CloudSolrClient client, String collectionName) throws SolrServerException, IOException {
		// TODO Auto-generated method stub
		UpdateResponse response=client.optimize(collectionName, true, true,30);
		System.out.println(response.toString());
		System.out.println(collectionName + " optimized!");
	}

public static void deleteDocs(CloudSolrClient client, String collectionName) throws SolrServerException, IOException {
		// TODO Auto-generated method stub
	client.deleteByQuery(collectionName, "*:*");
	client.commit(collectionName,true,true);
	
	}

public static void createCollection (SolrClient client, String collection, String configName,int shards,int replicas) throws IOException, SolrServerException {
    	
		try {
			SolrRequest<?> listReq=new CollectionAdminRequest.List() ;
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){

				Delete deleteReq = new Delete();
		    	deleteReq.setCollectionName(collection);
		    	client.request(deleteReq);
		    	//client.commit(collection,true, true);
		    	System.out.println(collection + " deleted. \n");
			
			}
			else
			{	
			}
	    	
    	} catch (SolrException e) {e.printStackTrace();}
    	
    	
		System.out.println("Creating collection " + collection + " ... ");
    	Create createReq = new Create();
    	createReq.setConfigName(configName);
    	createReq.setNumShards(shards);
    	
    	createReq.setMaxShardsPerNode(replicas);
    	createReq.setReplicationFactor(replicas);
    	createReq.setCollectionName(collection);
    	
    	client.request(createReq);
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	client.commit(collection,true,true);
    }

	
	public static void createCollection (SolrClient client, String collection, String configName) throws IOException, SolrServerException {
    	
		try {
			SolrRequest<?> listReq=new CollectionAdminRequest.List() ;
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){

				Delete deleteReq = new Delete();
		    	deleteReq.setCollectionName(collection);
		    	client.request(deleteReq);
		    	//client.commit(collection,true, true);
		    	System.out.println(collection + " deleted. \n");
			
			}
			else
			{	
			}
	    	
    	} catch (SolrException e) {e.printStackTrace();}
    	
    	
		System.out.println("Creating collection " + collection + " ... ");
    	Create createReq = new Create();
    	createReq.setConfigName(configName);
    	createReq.setNumShards(4);
    	
    	createReq.setMaxShardsPerNode(1);
    	createReq.setReplicationFactor(1);
    	createReq.setCollectionName(collection);
    	
    	client.request(createReq);
    	
    	client.commit(collection,true,true);
    }
public static void createCollection (SolrClient client, String collection, HashMap<String,String> params) throws IOException, SolrServerException {
    	
		try {
			SolrRequest<?> listReq=new CollectionAdminRequest.List() ;
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){

				Delete deleteReq = new Delete();
		    	deleteReq.setCollectionName(collection);
		    	client.request(deleteReq);
		    	//client.commit(collection,true, true);
		    	System.out.println(collection + " deleted. \n");
			
			}
			else
			{	
			}
	    	
    	} catch (SolrException e) {e.printStackTrace();}
    	
    	
		System.out.println("Creating collection " + collection + " ... ");
    	Create createReq = new Create();
    	String configName=params.get("configName");
    	String nodeSet=params.get("nodeSet");
    	createReq.setConfigName(configName);
    	createReq.setNumShards(2);
    	
    	createReq.setMaxShardsPerNode(1);
    	createReq.setReplicationFactor(1);
    	createReq.setCreateNodeSet(nodeSet);
    	createReq.setCollectionName(collection);
    	
    	Properties properties=new Properties();
    	properties.setProperty("dataDir", params.get("dataDir"));
    	createReq.setProperties(properties);
    	
    	client.request(createReq);
    	
    	//client.commit(collection,true,true);
    }
public static void createCollection (SolrClient client, String collection, String configName,String nodeSet) throws IOException, SolrServerException {
    	
		try {
			SolrRequest<?> listReq=new CollectionAdminRequest.List() ;
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){

				Delete deleteReq = new Delete();
		    	deleteReq.setCollectionName(collection);
		    	client.request(deleteReq);
		    	//client.commit(collection,true, true);
		    	System.out.println(collection + " deleted. \n");
			
			}
			else
			{	
			}
	    	
    	} catch (SolrException e) {e.printStackTrace();}
    	
    	
		System.out.println("Creating collection " + collection + " ... ");
    	Create createReq = new Create();
    	createReq.setConfigName(configName);
    	createReq.setNumShards(4);
    	
    	createReq.setMaxShardsPerNode(1);
    	createReq.setReplicationFactor(1);
    	createReq.setCreateNodeSet(nodeSet);
    	createReq.setCollectionName(collection);
    	
    	client.request(createReq);
    	
    	//client.commit(collection,true,true);
    }
	
public static void createCollection (SolrClient client, String collection, String configName,Integer numShards) throws IOException, SolrServerException {
    	
		try {
			SolrRequest<?> listReq=new CollectionAdminRequest.List() ;
			NamedList<Object> qr=client.request(listReq);
			
			List<String> collections=(List<String>) qr.get("collections");
			if (collections.contains(collection)){

				Delete deleteReq = new Delete();
		    	deleteReq.setCollectionName(collection);
		    	client.request(deleteReq);
		    	//client.commit(collection,true, true);
		    	System.out.println(collection + " deleted. \n");
			
			}
			else
			{	
			}
	    	
    	} catch (SolrException e) {e.printStackTrace();}
    	
    	
		System.out.println("Creating collection " + collection + " ... ");
    	Create createReq = new Create();
    	createReq.setConfigName(configName);
    	createReq.setNumShards(numShards);
    	
    	createReq.setMaxShardsPerNode(1);
    	createReq.setReplicationFactor(1);
    	createReq.setCollectionName(collection);
    	
    	client.request(createReq);
    	
    	client.commit(collection,true,true);
    }
	
    public void createCore(String solrDir, String solrCore, SolrClient server) throws IOException, SolrServerException {
        File dest = new File(new File(solrDir), solrCore);
        File conf = new File(dest, "conf");
        if (!conf.exists()) {
            File src = new File(new File(solrDir), "configsets/data_driven_schema_configs/subdocConf");
            FileUtils.copyDirectory(src, conf);
            CoreAdminRequest.createCore(solrCore, new File(new File(solrDir), solrCore).getAbsolutePath(), server);
            
        }
    }
	
}
