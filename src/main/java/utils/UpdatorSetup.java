package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

public class UpdatorSetup {
	protected final WKTReader reader=new WKTReader();
	protected String env;
	protected HashMap<Integer,Geometry> submtzs;
	protected HashMap<Integer,List<Integer>> hierarchies;
	protected HashMap<Integer, Point> buildings;
	protected String quarter;
	protected String year;
	protected String colName;
	
	public void getBuildings() throws IOException, SolrServerException, ParseException
	{
		HashMap<Integer, Point> buildingMap=new HashMap<Integer, Point>();
		SolrClient client=getSolrClient("buildings");
		SolrQuery query=new SolrQuery();
		query.set("q", "*:*");
		query.set("fl", "id_ti,location_srpt,building_type_s");
		query.setRows(1000);
		
		QueryResponse qr=client.query(query,METHOD.GET);
		SolrDocumentList list=qr.getResults();
		
		Integer id;
		//String str;
		for (SolrDocument doc:list)
		{	
			id=(Integer) doc.getFieldValue("id_ti");
			String ptStr=(String) doc.getFieldValue("location_srpt");
			if (ptStr!=null){
				Point pt=(Point) reader.read(ptStr);
				//System.out.println(ptStr + "\n" + str);
				buildingMap.put(id, pt);
			}
			
		}
		
		this.buildings=buildingMap;
		System.out.println("There are " + buildings.size() + " buildings found!\n");
		
		client.close();
		
	}
	
	public void getSubmtzs() throws SolrServerException, IOException, ParseException{
		HashMap<Integer,Geometry> submtzMap=new HashMap<Integer,Geometry>();
		//WKTReader reader=new WKTReader();
		SolrClient client=getSolrClient("geofeatures");
		
		SolrQuery qr=new SolrQuery();
		qr.add("qt","/select");
		qr.add("q","id_ti:*");
		qr.add("fq","type_s:sub-mtz");
		
		qr.setRows(2000);
		qr.setStart(0);
		qr.setFields("id_ti", "location_srpt");
		
		
		QueryResponse res=client.query(qr);
		SolrDocumentList docList=res.getResults();
		System.out.println(docList.size() + " results found for submtzs!");
		
		String location;
		Integer id;
		for (SolrDocument doc: docList)
		{
			id= (Integer) doc.getFieldValue("id_ti");
			location= (String) doc.getFieldValue("location_srpt");
		
			Geometry g=reader.read(location);
			submtzMap.put(id, g);
		}
		
		
		this.submtzs=submtzMap;
		
		client.close();
	}
	
	public SolrClient getSolrClient(String coreName)
	{
		String hostUrl="http://52.74.153.22:8983/solr/" + coreName;
		
		if (env!=null && env.equalsIgnoreCase("local")){
			hostUrl="http://localhost:8983/solr/" + coreName;
		}
		SolrClient client=new HttpSolrClient(hostUrl);
		
		return client;
	}
	
	public ConcurrentUpdateSolrClient getConcurrentSolrClient(String coreName, int queueSize,int threadCount)
	{
		String hostUrl="http://52.74.153.22:8983/solr/" + coreName;
		if (env.equalsIgnoreCase("local")){
			hostUrl="http://localhost:8983/solr/" + coreName;
		}
		ConcurrentUpdateSolrClient client=new ConcurrentUpdateSolrClient(hostUrl,queueSize,threadCount);
		return client;
	}
	
	public CloudSolrClient getCloudSolrClient(String zkhost)
	{
		//String zkhost="52.76.80.91,52.76.58.188,52.76.76.186:2181";
		//zkhost="52.65.54.15,52.65.103.71,52.65.103.73:2181";
		CloudSolrClient client =new CloudSolrClient(zkhost);
		client.setParallelUpdates(true);
		return client;
	}
	
	
	public void setEnv(String environ)
	{
		this.env=environ;
	}
	
	
	public void getHierarchy() throws IOException{
		
		hierarchies=new HashMap<Integer,List<Integer>>();
		InputStream is=this.getClass().getResourceAsStream("/submtzs.csv");
		BufferedReader br=new BufferedReader(new InputStreamReader(is));
		String line;
		while (( line=br.readLine()) !=null)
		{
			//System.out.println(line);
			String[] arr=line.split(",");
			if (arr[0].length()==0 || arr[3].equalsIgnoreCase("N.A."))
			{
				continue;
			}
			Integer submtz=Integer.valueOf(arr[0]);
			Integer subzone=Integer.valueOf(arr[3]);
			Integer planningArea=Integer.valueOf(arr[6]);
			Integer planningRegion=Integer.valueOf(arr[9]);
			List<Integer> ls=new ArrayList<Integer>();
			ls.add(subzone);
			ls.add(planningArea);
			ls.add(planningRegion);
			hierarchies.put(submtz, ls);
			
		}
		System.out.println(hierarchies.size() + " submtzs with hierarchy level!");
		//hierarchies.put(427, Arrays.asList(83,60,1));
		hierarchies.put(1184, Arrays.asList(0,0,0));
		//hierarchies.put(1278, Arrays.asList(329,12,2));
		hierarchies.put(1530, Arrays.asList(0,0,0));
		/*
		for (Integer s: hierarchies.keySet())
		{
			System.out.println(s + "\t"+hierarchies.get(s));
		}
		*/
		
		
		br.close();
		
	}
	
	public Integer getSubmtzIdByPoint(Point location) throws ParseException, SolrServerException, IOException
	{
		if (submtzs==null){
			this.getSubmtzs();
		}
						
		Integer submtz=null;
		//Geometry lg=reader.read(location);
		
		for (Integer id: submtzs.keySet())
		{
			Geometry bg=submtzs.get(id);
			
			if (bg.covers(location))
			{
				submtz=id;
				//return submtz;
			}
		}
		
		return submtz;
	}
	
	
	
	public Integer getBuildingIdByLatLng(String latLng) throws IOException, SolrServerException, ParseException
	{	
		if (buildings==null)
		{
			this.getBuildings();
		}
		Integer buildingId=null;
		double minDistance=0.10;
		String[] parts=latLng.split(",");
		if (parts.length==1){
			throw new ParseException("Please input a latLng string in the format of 'latitude,longitude'!");
		}
		Double d1=Double.parseDouble(parts[0]);
		Double d2=Double.parseDouble(parts[1]);
		
		
		DistanceCalculator cal1=new DistanceCalculator();
		for (Integer id:buildings.keySet())
		{
			Coordinate locs=buildings.get(id).getCoordinate();
			Double loc1=locs.y;
			Double loc2=locs.x;
			double d3=cal1.distance(d1, d2, loc1, loc2, 'K');
			if (d3 <= 0.10 && d3 < minDistance  )
			{
				minDistance=d3;
				buildingId=id;
			}
		}
		
		return buildingId;
	}

	/**
	 * @return the quarter
	 */
	public String getQuarter() {
		return quarter;
	}

	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the colName
	 */
	public String getColName() {
		return colName;
	}

	/**
	 * @param colName the colName to set
	 */
	public void setColName(String colName) {
		this.colName = colName;
	}
	
	
	
	
}
