package utils;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CoreAdminRequest;

public class CoreUtils {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SolrClient client=new HttpSolrClient("http://52.74.153.22:8983/solr/");
		//unloadCore(client,"routes-MHA-Q2-2015");
		createCore(client,"routes-MHA-Q3-2015","/solr-vol/solr-6.0.0/server/solr/routes_Q3_2015_MHA/","conf/solrconfig.xml");
	}	

	
	public static void createCore(SolrClient client,String coreName,String instanceDir,String config){
		
		CoreAdminRequest.Create createRequest=new CoreAdminRequest.Create();
		createRequest.setCoreName(coreName);
		createRequest.setInstanceDir(instanceDir);
		try {
			client.request(createRequest);
			System.out.println(coreName + " created successfully!" );
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	public static void unloadCore(SolrClient client,String coreName){
	
		CoreAdminRequest.Unload deleteRequest=new CoreAdminRequest.Unload(false);
		try {
			deleteRequest.setCoreName(coreName);
			client.request(deleteRequest);
			System.out.println(coreName + " deleted successfully!" );
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
